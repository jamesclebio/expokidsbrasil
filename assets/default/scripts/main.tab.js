var main = main || {};

;(function($, window, document, undefined) {
  'use strict';

  main.tab = {
    settings: {
      autoinit: true,
      main: '.main-tab',
      mainIndex: '.main-tab-index',
      mainContent: '.main-tab-content',
      onClass: 'on'
    },

    init: function() {
      if ($(this.settings.main).length) {
        this.handler();
        this.builder();
      }
    },

    handler: function() {
      var that = this,
          $main = $(this.settings.main),
          $mainIndex = $main.find(this.settings.mainIndex);

      // Main index
      $mainIndex.find('a').on({
        click: function(e) {
          that.change($(this));
          e.preventDefault();
        }
      });
    },

    builder: function() {
      var $main = $(this.settings.main),
          $mainIndex = $main.find(this.settings.mainIndex);

      $mainIndex.find('li:first-child a').trigger('click');
    },

    change: function($this) {
      var $main = $this.closest(this.settings.main),
          $mainContent = $main.find(this.settings.mainContent),
          $linkParent = $this.closest('li');

      $linkParent.siblings().removeClass(this.settings.onClass);
      $linkParent.addClass(this.settings.onClass);

      $mainContent.hide();
      $($this.attr('href')).fadeIn(150);
    }
  };
}(jQuery, this, this.document));
