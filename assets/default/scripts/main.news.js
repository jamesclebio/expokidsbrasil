var main = main || {};

;(function($, window, document, undefined) {
    'use strict';

    main.news = {
        settings: {
            autoinit: true,
            main: '.news',
            navTemplate: '<div class="news-nav"><a href="#" class="prev"></a><a href="#" class="next"></a></div>'
        },

        init: function() {
            if ($(this.settings.main).length) {
                this.builder();
            }
        },

        builder: function() {
            var $main = $(this.settings.main);

            $main
                .append(this.settings.navTemplate)
                .find('.news-items').carouFredSel({
                    items: {
                        visible: 4,
                        minimum: 5
                    },
                    scroll: {
                        fx: 'scroll',
                        items: 1
                    },
                    auto: {
                        play: false,
                        timeoutDuration: 4000
                    },
                    prev: $main.find('.news-nav .prev'),
                    next: $main.find('.news-nav .next')
                });
        }
    };
}(jQuery, this, this.document));
