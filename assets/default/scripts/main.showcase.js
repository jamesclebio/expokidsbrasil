var main = main || {};

;(function($, window, document, undefined) {
    'use strict';

    main.showcase = {
        settings: {
            autoinit: true,
            main: '.showcase',
            mainIndex: '.showcase-index',
            mainGrid: '.showcase-grid',
            onClass: 'on',
            loadingTemplate: '<div class="block-loading">Carregando produtos...</div>',
            errorTemplate: '<div class="alert-short alert-short-danger"><p>Ops! Algo deu errado... :(</p></div>'
        },

        init: function() {
            if ($(this.settings.main).length) {
                this.handler();
                this.builder();

            }
        },

        handler: function() {
            var that = this,
                $main = $(this.settings.main),
                $mainIndex = $(that.settings.mainIndex),
                $mainGrid = $(this.settings.mainGrid);

            // Main index
            $mainIndex.find('a').on({
                click: function(e) {
                    that.grid($(this));
                    e.preventDefault();
                }
            });

            // Main grid index
            $mainGrid.on({
                change: function(e) {
                    that.gallery($(this).val());
                    e.preventDefault();
                }
            }, '.index select');
        },

        builder: function() {
            var that = this,
                $main = $(this.settings.main),
                $mainIndex = $(this.settings.mainIndex);

            $mainIndex.find('.' + that.settings.onClass + ' a').trigger('click');
        },

        grid: function($this) {
            var that = this,
                $main = $(this.settings.main),
                $mainIndex = $(this.settings.mainIndex),
                $mainGrid = $(this.settings.mainGrid),
                url = $this.attr('href');

            $.ajax({
                type: 'GET',
                url: url,

                beforeSend: function() {
                    $mainGrid.empty().append(that.settings.loadingTemplate);
                },

                error: function() {
                    $mainGrid.empty().append(that.settings.errorTemplate);
                },

                success: function(data) {
                    var $mainGridIndex;

                    $mainGrid.empty().append(data);
                    basis.init('fieldSelect');

                    $mainGridIndex = $mainGrid.find('.index');
                    $mainGridIndex.find('h4').text('Opções para ' + $this.closest('li').text().toLowerCase() + ':');
                    $mainGridIndex.find('select').trigger('change');

                    $mainIndex.find('li').removeClass(that.settings.onClass);
                    $this.closest('li').addClass(that.settings.onClass);
                }
            });
        },

        gallery: function(url) {
            var that = this,
                $main = $(this.settings.main),
                $mainGrid = $(this.settings.mainGrid),
                $gallery = $mainGrid.find('.gallery');

            $.ajax({
                type: 'GET',
                url: url,

                beforeSend: function() {
                    $gallery.empty().append(that.settings.loadingTemplate);
                },

                error: function() {
                    $gallery.empty().append(that.settings.errorTemplate);
                },

                success: function(data) {
                    $gallery.empty().append(data);
                    basis.rollHash.init();
                    main.lightbox.init();
                    // basis.initTrigger(that, 'lightbox');
                }
            });
        }
    };
}(jQuery, this, this.document));
