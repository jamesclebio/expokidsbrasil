var main = main || {};

;(function($, window, document, undefined) {
    'use strict';

    main.billboard = {
        settings: {
            autoinit: true,
            main: '.billboard',
            indexTemplate: '<div class="billboard-index-wrapper"><div class="billboard-index" /></div>'
        },

        init: function() {
            if ($(this.settings.main).length) {
                this.builder();
            } else {
                basis.rollHash.init();
            }
        },

        builder: function() {
            var that = this,
                $main = $(this.settings.main),
                $container = $main.find('.billboard-container'),
                init = new Image();

            //  Bug prevent for basis rollHash (scroll)
            init.src = $container.find('.item:first-child img').attr('src');

            init.onload = function() {
                $main.show().append(that.settings.indexTemplate);

                $container.carouFredSel({
                    responsive: true,
                    items: {
                        height: 'variable',
                        visible: 1,
                        minimum: 2
                    },
                    scroll: {
                        fx: 'crossfade',
                        pauseOnHover: false
                    },
                    auto: {
                        timeoutDuration: 4000
                    },
                    pagination: $main.find('.billboard-index')
                });

                basis.rollHash.init();
            };

        }
    };
}(jQuery, this, this.document));
