;(function($, window, document, undefined) {
	'use strict';

	basis.modal = {
		settings: {
			autoinit: true
		},

		wrapper: '[data-modal]',
		parent: '.modal',
		parent_template: '<div class="modal" style="display: none;"><div class="modal-container"><div class="modal-box"><button type="button" class="modal-close" data-modal-close></button></div></div></div>',

		open: function(url) {
			var	that = this,
				$parent,
				loading_class = 'modal-loading';

			$('body').append(that.parent_template);
			$parent = $(that.parent);

			$parent.fadeIn(100, function() {
				var	$container = $(this).find('.modal-box');

				$.ajax({
					url: url,

					beforeSend: function() {
						$container.addClass(loading_class);
					},

					error: function() {
						alert('Ops! Algo deu errado... :(');

						that.close();
					},

					success: function(data) {
						$container.removeClass(loading_class).prepend(data);

                        // Custom (temp)
                        basis.init('browser', 'share');
                        main.lightbox.init();
					}
				});
			});
		},

		close: function() {
			var	that = this,
				$wrapper = $(that.parent);

			$wrapper.fadeOut(100, function() {
				$(this).remove();
			});
		},

		bindings: function() {
			var	that = this,
				$wrapper = $(that.parent);

			// Open
			$(document).on({
				click: function(e) {
					var url = $(this).attr('href');

					that.open(url);
					e.preventDefault();
				}
			}, '[data-modal]');

			// Close
			$(document).on({
				click: function(e) {
					that.close();
					e.preventDefault();
				}
			}, that.parent + ' [data-modal-close]');

			$(document).on({
				click: function() {
					that.close();
				}
			}, that.parent);

			$(document).on({
				keydown: function(e) {
					switch (e.which) {
						case 27:
							that.close();
							break;
					}
				}
			});

			// Box stop propagation
			$(document).on({
				click: function(e) {
					e.stopPropagation();
				}
			}, that.parent + ' .modal-box');
		},

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.bindings();
			}
		}
	};
}(jQuery, this, this.document));
