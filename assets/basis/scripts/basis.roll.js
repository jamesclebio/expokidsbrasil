;(function($, window, document, undefined) {
	'use strict';

	basis.roll = {
		settings: {
			main: 'html, body'
		},

		to: function(target) {
			var	that = this,
				$main = $(that.settings.main),
				$target = $(target);

			if (!$target.length) {
				$target = $main;
			}

			$main.animate({
				scrollTop: $target.offset().top - 79
			});
		}
	};
}(jQuery, this, this.document));

