;(function($, window, document, undefined) {
  'use strict';

  basis.fieldSelect = {
    settings: {
      autoinit: true,
      main: '[data-field-select]'
    },

    init: function() {
      if ($(this.settings.main).length) {
        this.builder();
      }
    },

    builder: function() {
      var $main = $(this.settings.main);

      $main.each(function () {
        $(this).selectize({
          allowEmptyOption: true
        });
      });

    }
  };
}(jQuery, this, this.document));
