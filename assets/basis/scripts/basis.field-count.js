;(function($, window, document, undefined) {
	'use strict';

	basis.fieldCount = {
		settings: {
			autoinit: true
		},

		wrapper: '[data-field-count]',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$wrapper.each(function() {
				var	limit = $(this).attr('maxlength'),
					counter_template = '<span class="field-count-counter">' + limit + '</span>';

				$(this).after(counter_template);
				$(this).next().data('limit', limit);

				that.count($(this));
			});
		},

		count: function($this) {
			var	$counter = $this.next(),
				limit = $counter.data('limit'),
				field_length = $this.val().length;

			if ((limit - field_length) > limit) {
				$this.val($this.val().substr(limit));
				field_length = $this.val().length;
			}

			$counter.text(limit - field_length);
		},

		bindings: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$wrapper.on({
				keyup: function() {
					that.count($(this));
				},

				change: function() {
					that.count($(this));
				}
			});
		},

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.build();
				that.bindings();
			}
		}
	};
}(jQuery, this, this.document));

