<div class="heading-page">
    <h2>Form</h2>
</div>

<form action="" class="form">
	<div class="action-sticky">
		<div class="action-sticky-container">
			<button type="submit" name="_save" class="button button-success">Save</button>
			<button type="submit" name="_continue" class="button">Save and continue editing</button>
			<button type="submit" name="_addanother" class="button">Save and add another</button>
			<a href="delete/" class="button button-danger" title="Delete"><span class="icon-trash-a"></span></a>
		</div>
	</div>

	<fieldset class="field-group">
		<legend>Mask</legend>

		<label>Date<input name="" type="text" data-field-mask="date"></label>
		<label>Time<input name="" type="text" data-field-mask="time"></label>
		<label>Time long<input name="" type="text" data-field-mask="time-long"></label>
		<label>Datetime<input name="" type="text" data-field-mask="datetime"></label>
		<label>Datetime long<input name="" type="text" data-field-mask="datetime-long"></label>
		<label>CEP<input name="" type="text" data-field-mask="cep"></label>
		<label>Phone<input name="" type="text" data-field-mask="phone"></label>
		<label>CNPJ<input name="" type="text" data-field-mask="cnpj"></label>
		<label>CPF<input name="" type="text" data-field-mask="cpf"></label>
		<label>Currency real<input name="" type="text" data-field-mask="currency-real" value="0"></label>
		<label>Currency dollar<input name="" type="text" data-field-mask="currency-dollar" value="0"></label>
		<label>Weight<input name="" type="text" data-field-mask="weight" value="0"></label>
	</fieldset>

	<fieldset class="field-group">
		<legend>Picker</legend>

		<label>Date<input name="" type="text" data-field-picker="date" data-field-mask="date"></label>
		<label>Time<input name="" type="text" data-field-picker="time" data-field-mask="time"></label>
		<label>Datetime<input name="" type="text" data-field-picker="datetime" data-field-mask="datetime"></label>
	</fieldset>

	<fieldset class="field-group">
		<legend>Select custom</legend>

		<select data-field-select name="">
			<option value="" disabled>Select</option>
			<option value="1" selected>Option 1</option>
			<option value="2">Option 2</option>
		</select>
	</fieldset>

	<fieldset class="field-group">
		<legend>Extend 1</legend>

		<label>Field 1
			<select name="field1" data-field-extend="__index">
				<option value="" disabled>Select</option>
				<option value="1" selected>Option 1</option>
				<option value="2">Option 2</option>
			</select>
		</label>
	</fieldset>

	<fieldset class="field-group">
		<legend>Extend 2</legend>

		<label>Field 1 *<input name="" type="text" data-field-extend="field1|1" class="field-danger" required>
			<span class="field-helper-info">Note text</span>
			<ul class="field-helper-alert field-helper-alert-danger">
				<li>Error description 1</li>
			</ul>
		</label>

		<label>Field 2
			<select name="field2" data-field-extend="__index|field1|2">
				<option value="" disabled selected>Select</option>
				<option value="1">Option 1</option>
				<option value="2">Option 2</option>
			</select>
		</label>
	</fieldset>

	<fieldset class="field-group">
		<legend>Extend 3</legend>

		<label>Field 1<input name="" type="text" data-field-extend="field2|1"></label>
		<label>Field 2<input name="" type="text" data-field-extend="field2|1"></label>
	</fieldset>
</form>

