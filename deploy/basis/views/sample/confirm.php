<div class="heading-page">
    <h2>Confirm</h2>
</div>

<form action="">
	<div class="alert alert-icon">
		<h4 class="heading">Confirm action</h4>

		<div class="block-group text">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit delectus ducimus molestias ab ratione nihil quibusdam officia esse ullam, numquam quae, aliquid quasi quo repudiandae, mollitia saepe. Recusandae, sed cumque!</p>
			<ul>
				<li>Lorem: <a href="#">ipsum dolor</a></li>
				<li>Lorem: <a href="#">ipsum dolor</a></li>
				<li>Lorem: <a href="#">ipsum dolor</a></li>
			</ul>
		</div>

		<div class="block-group block-action">
			<button type="button" class="button" data-browser="back">Cancelar</button>
			<button type="submit" class="button button-warning">Continue</button>
		</div>
	</div>
</form>

