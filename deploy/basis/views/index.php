<!-- Home (billboard) -->
<section class="section-page" id="home">
  <div class="billboard">
    <div class="billboard-container">
      <div class="item item-center">
        <img src="<?php echo $this->_asset('default/images/billboard/01.jpg'); ?>" alt="" class="image">
      </div>
    </div>
  </div>
</section>

<!-- O evento -->
<div class="section-page" id="evento">
  <div class="section-page-container">
    <header class="section-page-header">
      <h2 class="main-heading">O evento</h2>
    </header>

    <div class="section-page-content">
      <div class="grid grid-items-2">
        <div class="grid-item">
          <img src="<?php echo $this->_asset('default/images/logo-expokidsbrasil-2.png'); ?>" alt="">
        </div>
        <div class="grid-item">
          <div class="text">
            <p>A Expo World Kids Brasil é um evento que reúne os melhores serviços para festas infantis. Além de estar em contato direto com diversos expositores, os visitantes também tem a chance de entender melhor esse mercado, conhecendo de perto fornecedores, marcas e tendências. Tudo isso aliado à praticidade de resolver todos os detalhes da festa em um só lugar. Em 2015 o evento será realizado em shoppings com grande circulação de Recife, Salvador e Fortaleza.</p>
            <img src="<?php echo $this->_asset('default/images/evento.jpg'); ?>" alt="" class="responsive">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Programação -->
<div class="section-page" id="programacao">
  <div class="section-page-container">
    <header class="section-page-header">
      <h2 class="main-heading">Programação</h2>
    </header>

    <div class="section-page-content"></div>
  </div>
</div>

<!-- Galeria -->
<div class="section-page main-gallery" id="galeria">
  <div class="section-page-container">
    <header class="section-page-header">
      <h2>Confira fotos de nossos eventos passados e sonhe com o desse ano.</h2>
      <div class="main-gallery-index">
        <ul class="list-colorbutton">
          <li class="color-4 on"><a href="#galeria-recife">Recife</a></li>
          <li class="color-3"><a href="#galeria-fortaleza">Fortaleza</a></li>
          <li class="color-5"><a href="#galeria-salvador">Salvador</a></li>
        </ul>
      </div>
    </header>

    <div class="section-page-content">
      <div class="main-gallery-content" id="galeria-recife">
        <div class="main-gallery-albums">
          <h5>Selecione o álbum</h5>
          <ul>
            <li class="on"><a href="<?php echo $this->_url('gallery/view/album/recife-semana-2'); ?>">Expo World Kids Brasil em Recife (segunda semana)</a></li>
            <li><a href="<?php echo $this->_url('gallery/view/album/recife-semana-1'); ?>">Expo World Kids Brasil em Recife (primeira semana)</a></li>
            <li><a href="<?php echo $this->_url('gallery/view/album/recife-lancamento'); ?>">Lançamento Expo World Kids Brasil em Recife</a></li>
          </ul>
        </div>

        <div class="main-gallery-thumbs"></div>
      </div>

      <div class="main-gallery-content" id="galeria-fortaleza">
        <div class="main-gallery-albums">
          <h5>Selecione o álbum</h5>
          <ul>
            <li class="on"><a href="<?php echo $this->_url('gallery/view/album/fortaleza-lancamento'); ?>">Lançamento Expo World Kids Brasil em Fortaleza</a></li>
          </ul>
        </div>

        <div class="main-gallery-thumbs"></div>
      </div>

      <div class="main-gallery-content" id="galeria-salvador">
        <div class="main-gallery-albums">
          <h5>Selecione o álbum</h5>
          <ul>
            <li class="on"><a href="<?php echo $this->_url('gallery/view/album/salvador-semana-2'); ?>">Expo World Kids Brasil em Salvador (segunda semana)</a></li>
            <li><a href="<?php echo $this->_url('gallery/view/album/salvador-semana-1'); ?>">Expo World Kids Brasil em Salvador (primeira semana)</a></li>
            <li><a href="<?php echo $this->_url('gallery/view/album/salvador-lancamento'); ?>">Lançamento Expo World Kids Brasil em Salvador</a></li>
          </ul>
        </div>

        <div class="main-gallery-thumbs"></div>
      </div>
    </div>
  </div>
</div>

<!-- Fornecedores -->
<div class="section-page" id="fornecedores">
  <div class="section-page-container">
    <header class="section-page-header">
      <h2 class="main-heading">Fornecedores</h2>
    </header>

    <div class="section-page-content">
      <div class="text-intro text">
        <p><strong>Tenha a sua disposição os melhores profissionais do segmento de festas infantis, incluindo:</strong></p>
        <p>Fotografia, buffet, animação, fantasias, salões e espaços para festas, decoração, aluguel de mobiliário, brinquedos, brindes e lembrancinhas, bolos, doces, guloseimas, salgados e chocolates personalizados, estrutura de som e DJ, entre outros.</p>
      </div>

      <div class="main-tab">
        <div class="main-tab-index">
          <ul class="list-colorbutton">
            <li class="color-4"><a href="#fornecedores-recife">Recife</a></li>
            <li class="color-3"><a href="#fornecedores-fortaleza">Fortaleza</a></li>
            <li class="color-5"><a href="#fornecedores-salvador">Salvador</a></li>
          </ul>
        </div>

        <div class="main-tab-content color-4" id="fornecedores-recife">
          <div class="main-tab-content-container">
            <div class="providers">
              <div class="block-provider">
                <h6>FREITAS CHOCOLATERIE</h6>
                Tels: 081-33385163 - 081-998350003<br>
                Instagram: @chefthiagofreitas<br>
                Facebook: Freitas chocolaterie<br>
                E-mail: freitaschocolaterie@hotmail.com<br>
                Site: freitaschocolaterie.com.br
              </div>
              <div class="block-provider">
                <h6>BUFFET JUNGLES KIDS</h6>
                Tel: 3269-7087 / 3221-0259<br>
                E-mail: junglekidscasarao@hotmail.com<br>
                Facebook: buffetjungleskids<br>
                Instagram: @buffetjungleskids<br>
                Site: www.buffetjunglekids.com.br
              </div>
              <div class="block-provider">
                <h6>ESPAÇO DE EVENTOS VILLA PONTE D'UCHÔA RECEPÇÕES</h6>
                Tel: 3427-9025 / 98898-8533<br>
                Instagram: villaponteduchoa<br>
                Facebook: villaponteduchoa<br>
                E-mail: eventos@villaponteduchoa.com.br<br>
                Site: www.villaponteduchoa.com.br
              </div>
              <div class="block-provider">
                <h6>BUFFET LÍQUIDO - BALACA BUFFET DE LÍQUIDOS</h6>
                Tel: (81) 3031 2815 / (81) 9 9984 1790 / (81) 9 8795 1626<br>
                Redes Sociais: https://www.facebook.com/balacakids<br>
                Instagram: @balacakids<br>
                E-mail: balaca@balaca.com.br ou balacakids@balaca.com.br<br>
                Site: www.balaca.com.br
              </div>
              <div class="block-provider">
                <h6>DOCES - VERA BARROS</h6>
                Tel: 9810-6775 / 9680-1940<br>
                Instagram: verabarrosbeijosfinos
              </div>
              <div class="block-provider">
                <h6>KAIRE BALLOONS</h6>
                Tel: 81 3236-5074<br>
                Redes Sociais: @kaireballoons<br>
                E-mail: contato@kaireballoons.com<br>
                Site: www.kaireballoons.com
              </div>
              <div class="block-provider">
                <h6>DECORAÇÃO - DICUORE FESTAS</h6>
                Tel: 81 9 9938-3333<br>
                E-mail: dicuore.festas@hotmail.com<br>
                Instagram: dicuorefestas
              </div>
              <div class="block-provider">
                <h6>FILMAGEM - CORUJA CURIOSA</h6>
                Tel: 99836-4889<br>
                E-mail: corujacuriosaeventos@gmail.com<br>
                Instagram: @corujacuriosa<br>
                Facebook: Coruja Curiosa
              </div>
              <div class="block-provider">
                <h6>DECORAÇÃO - PERYLAMPO</h6>
                Tel: 81. 997223555 | 81.996650671<br>
                Instagram: @perylampofestas<br>
                Facebook: perylampofestas<br>
                E-mail: mariana@perylampo.com.br<br>
                Site: www.perylampo.com.br
              </div>
              <div class="block-provider">
                <h6>DECORAÇÃO - DONNA CAMÉLIA FESTAS</h6>
                Tel: 984320700 / 996481191 (whatsapp)<br>
                Facebook: Donna Camélia festas<br>
                Instagram: @donnacameliafestas<br>
                E-mail: donnacamelia@hotmail.com
              </div>
              <div class="block-provider">
                <h6>BUFFET - DONNA FESTA</h6>
                Tel: 81 30366567 / 988630156 / 92520621 / 91605904<br>
                Instagram: @donna_festa<br>
                Facebook: Donna Festa<br>
                E-mail: contato@donnafesta.com.br<br>
                Site: www.donnafesta.com.br
              </div>
              <div class="block-provider">
                <h6>LOURINHO FILMES</h6>
                Tel: 81 30822844 / 988143393 / 999463464<br>
                Instagram: @lourinhofilmes<br>
                Facebook: Lourinho Filmes<br>
                E-mail: contatp@lourinhofilmes.com.br<br>
                Site: www.lourinhofilmes.com.br
              </div>
              <div class="block-provider">
                <h6>REGINA COELI FOTOGRAFIA</h6>
                Tel: 81 - 99964 4484<br>
                Face – Regina Coeli Fotografia
                Instagram: reginacoelifotografia<br>
                E-mail: reginacoeli.fotografia@hotmail.com<br>
                Site: www.reginacoelifotografia.com.br
              </div>
              <div class="block-provider">
                <h6>BUFFET -FAZENDO A FESTA</h6>
                Instagram: @fazendoafestarecife<br>
                Site- www.fazendoafestarecife.com.br
                E-mail -  contato@fazendoafestarecife.com.br
                Tel: (81) 30323362 - (81) 8428 0777
              </div>
              <div class="block-provider">
                <h6>MIMOS PARA LEMBRAR</h6>
                Tel: (81)8401-1197 Oi (81)9693-4386 Tim – Whatsapp<br>
                E-mail: mimosparalembrar@hotmail.com<br>
                Instagram: @mimosparalembrar<br>
                Site: mimosparalembrar.com.br
              </div>
              <div class="block-provider">
                <h6>PRA BRINCAR</h6>
                Tel: 8699-2311<br>
                E-mail: prabrinkar@hotmail.com<br>
                Instagram: @prabrinkar<br>
                Site: prabrinkar.com.br
              </div>
              <div class="block-provider">
                <h6>PETIT COMITÉ RECEPÇÕES</h6>
                Tel: 3034 4303 / 9 8190 7001<br>
                Redes Sociais: @petitcomite.pe<br>
                E-mail: contato@petitcomite.pe<br>
                Site: www.petitcomite.pe
              </div>
              <div class="block-provider">
                <h6>DEL MARE EVENTOS PERSONALIZADOS</h6>
                Tel:(81) 99928-3030 / (81) 3204-3031
                Email: delmareeventos@gmail.com<br>
                Instagram: @delmareeventos<br>
                Facebook: delmareeventos<br>
                Site: www.delmareeventos.com.br
              </div>
              <div class="block-provider">
                <h6>CUBO MÁGICO</h6>
                Tel:(81) 99722-0407 /(81) 99750-9430
                Email: contato@cubomagico.cm<br>
                Instagram: @cubomagicofotofilme<br>
                Facebook: cubomagicofotofilme<br>
                Site: cubomagico.cm
              </div>
              <div class="block-provider">
                <h6>LOCAÇÃO DE MOBILIÁRIO - CHER PARTI</h6>
                Tel. 81 3031-2046 | 98204-2046.
                Facebook: www.facebook/cherpartibylaismedeiros.com.br<br>
                Instagram: @cherparti<br>
                Email: contato@laismedeiros.com.br
              </div>
              <div class="block-provider">
                <h6>LOJA - TEM FESTA</h6>
                Tel: 3103-6502 - 99418-6306<br>
                Facebook: lojatemfesta<br>
                Instagram - ojatemfesta
                E-mail: contato@lojatemfesta.com.br<br>
                Site: www.lojatemfesta.com.br
              </div>
              <div class="block-provider">
                <h6>BRIGADEIRO GOURMET - O TABULEIRO</h6>
                Tel: 99991-1213 / 99432-4521<br>
                IG. @otabuleiro
                E-mail: o_tabuleiro@outlook.com
              </div>
              <div class="block-provider">
                <h6>DECORAÇÃO E PERSONALIZADOS - MARICOTAS ARTES</h6>
                Instagram: Maricotasartes<br>
                E-mail: maricotasartes@hotmail.com
              </div>
              <div class="block-provider">
                <h6>LAIS COSTA BOLO</h6>
                Tel: (81) 3465.4072 / 9188.1426<br>
                E-mail: felais@globo.com<br>
                IG. @laistavarescosta
                Site: bolosdalais.com.br
              </div>
              <div class="block-provider">
                <h6>DOCES -EMÍLIA MALTA</h6>
                Tel: www.emiliamalta.com.br<br>
                E-mail: emilia@emiliamalta.com.br<br>
                Instagram: emiliamaltabrigadeiros<br>
                Site: www.emiliamalta.com.br
              </div>
              <div class="block-provider">
                <h6>CORUJA CURIOSA – FILMAGEM</h6>
                Tel: 99836-4889<br>
                IG. @corujacuriosa
                Facebook: Coruja Curiosa<br>
                E-mail: corujacuriosaeventos@gmail.com
              </div>
              <div class="block-provider">
                <h6>BOLO - CAKE À PORTER</h6>
                E-mail: aporter.cake@gmail.com<br>
                Instagram: @cakeaporter<br>
                facebook: aportercake
              </div>
              <div class="block-provider">
                <h6>CHOCOLATE DO CÉU</h6>
                Tel: 081 9 9578-2202/ 98788-2218<br>
                E-mail: chocolatedoceu.recife@gmail.com<br>
                Instagram: Chocolatedoceu<br>
                Facebook: Chocolatedoceu<br>
                Site: www.chocolatedoceu.com.br
              </div>
              <div class="block-provider">
                <h6>FREITAS CHOCOLATERIE</h6>
                Tels: 081-33385163 -- 081-998350003<br>
                Instagram: @chefthiagofreitas<br>
                Facebook: Freitas chocolaterie<br>
                E-mail: freitaschocolaterie@hotmail.com<br>
                Site: freitaschocolaterie.com.br
              </div>
              <div class="block-provider">
                <h6>DECORAÇÃO - TUDO EM FESTA</h6>
                Telefones: (81) 3325.5655 ou 99927.2727<br>
                Site: www.tudoemfesta.com<br>
                E-mail: contato@tudoemfesta.com<br>
                Instagram: @tudo_em_festa<br>
                Facebook: /decoracaotudoemfesta
              </div>
              <div class="block-provider">
                <h6>PERSONALIZADOS - FÁBRICA DESIGN</h6>
                Telefones: (81) 99132-6058 ou 99661.8357<br>
                Site: www.fabricadesign.art.br<br>
                E-mail: contato@fabricadesign.art.br<br>
                Instagram: @fabricadesign<br>
                Facebook: /fabricadesignrecife
              </div>
              <div class="block-provider">
                <h6>BUFFET JUNGLES KIDS</h6>
                Tel: 3269-7087 / 3221.0259<br>
                E-mail: junglekidscasarao@hotmail.com<br>
                Facebook: buffetjungleskids<br>
                Instagram: @buffetjungleskids<br>
                Site: www.buffetjunglekids.com.br
              </div>
              <div class="block-provider">
                <h6>KAIRE BALLOONS</h6>
                Tel: 81 3236-5074<br>
                Redes Sociais: @kaireballoons<br>
                E-mail: contato@kaireballoons.com<br>
                Site: www.kaireballoons.com
              </div>
              <div class="block-provider">
                <h6>DECORAÇÃO - LAB PARTY</h6>
                Tels: 9662-3739 / 9633-4503<br>
                IG. @labparty
                E-mail: larissareis@hotmail.com
              </div>
              <div class="block-provider">
                <h6>ANIMAÇÃO - DECOLORES EVENTOS</h6>
                Tel: 81-99758551 (Pedro) / 81-96309385<br>
                Facebook: decoloreseventosrecife<br>
                instagram: @decoloreseventos<br>
                E-mail: contato@decoloreseventos.com.br<br>
                Site: www.decoloreseventos.com.br
              </div>
              <div class="block-provider">
                <h6>BUFFET - DONNA FESTA</h6>
                Tel: 81 30366567 / 988630156 / 92520621 / 91605904<br>
                Instagram: @donna_festa<br>
                Facebook: Donna Festa<br>
                E-mail: contato@donnafesta.com.br<br>
                Site: www.donnafesta.com.br
              </div>
              <div class="block-provider">
                <h6>LOURINHO FILMES</h6>
                Tel: 81 30822844 / 988143393 / 999463464<br>
                Instagram: @lourinhofilmes<br>
                Facebook: Lourinho Filmes<br>
                E-mail: contatp@lourinhofilmes.com.br<br>
                Site: www.lourinhofilmes.com.br
              </div>
              <div class="block-provider">
                <h6>DECORAÇÃO - DONNA CAMÉLIA FESTAS</h6>
                Tel: 984320700 / 996481191 (whatsapp)<br>
                Fece: Donna Camélia festas<br>
                Instagram: @donnacameliafestas<br>
                E-mail: donnacamelia@hotmail.com
              </div>
              <div class="block-provider">
                <h6>SHARESPOT - TOTEM INTERATIVO</h6>
                Tel: 3127.0660 / 9705.8285<br>
                E-mail: rafael@sharespot.com.br<br>
                Instagram: sigasharespot<br>
                Facebook: sharespot<br>
                Site: site.sharespot.com.br
              </div>
              <div class="block-provider">
                <h6>LOCAÇÃO DE MOBÍLIA -CHER PARTI</h6>
                Tel. 81 3031-2046 | 98204-2046.
                Facebook: www.facebook/cherpartibylaismedeiros.com.br<br>
                Instagram: @cherparti<br>
                Email: contato@laismedeiros.com.br
              </div>
              <div class="block-provider">
                <h6>DECORAÇÃO - LE PETIT FESTAS TEMÁTICAS</h6>
                Tel: 81 992063338 / 81 991763131<br>
                Instagram: @lepetit_festas<br>
                Facebook -Le Petit Festas Temáticas
                E-mail: lepetitfestastematicas@gmail.com
              </div>
              <div class="block-provider">
                <h6>REGINA COELI FOTOGRAFIA</h6>
                Tel: 81 - 99964 4484<br>
                Face – Regina Coeli Fotografia
                Instagram: reginacoelifotografia<br>
                E-mail: reginacoeli.fotografia@hotmail.com<br>
                Site: www.reginacoelifotografia.com.br
              </div>
              <div class="block-provider">
                <h6>CHOCOLATE DO CÉU</h6>
                Tel: 081 9 9578-2202/ 98788-2218<br>
                E-mail: chocolatedoceu.recife@gmail.com<br>
                Instagram: Chocolatedoceu<br>
                Facebook: Chocolatedoceu<br>
                Site: www.chocolatedoceu.com.br
              </div>
              <div class="block-provider">
                <h6>MARIANA PARINI DOLCI & CIOCCOLATO</h6>
                Tel: 81 8801 4571 / 81 3269 4471 / 81 3034 0606<br>
                IG. @marianaparini_insta
              </div>
              <div class="block-provider">
                <h6>ARTIGOS DE CECORAÇÃO DE FESTA -CELEBRART</h6>
                Tel: (81) 3314.7988<br>
                Facebook: celebrart<br>
                Instagram-@celebrart
                E-mail: celebrart@celebrart.com.br<br>
                Site: celebrart.com.br
              </div>
              <div class="block-provider">
                <h6>DEL MARE EVENTOS PERSONALIZADOS</h6>
                Tel:(81) 99928-3030 / (81) 3204-3031
                Email: delmareeventos@gmail.com<br>
                Instagram: @delmareeventos<br>
                Facebook: delmareeventos<br>
                Site: www.delmareeventos.com.br
              </div>
              <div class="block-provider">
                <h6>CUBO MÁGICO</h6>
                Tel:(81) 99722-0407 /(81) 99750-9430
                Email: contato@cubomagico.cm<br>
                Instagram: @cubomagicofotofilme<br>
                Facebook: cubomagicofotofilme<br>
                Site: cubomagico.cm
              </div>
              <div class="block-provider">
                <h6>LOCAÇÃO DE OBJETOS E MÓVEIS - LOC&DECORE</h6>
                Tel: 971046006/34456006/986947943<br>
                Instagram @locdecore
                Facebook loc.decore
                E-mail: loc.decore@gmail.com
              </div>
              <div class="block-provider">
                <h6>DELICAT DOCES</h6>
                Tels: 3039-3443 / 99965-5805<br>
                Redes sociais: intagram @delicatdoces<br>
                E-mail: contato@delicatdoces.com.br<br>
                Site: www.delicatdoces.com.br
              </div>
              <div class="block-provider">
                <h6>DOCES -DELICI ART</h6>
                Tel: 081 991976744/ 081 81548519<br>
                E-mail - tassyla_kezia@hotmail.com
                Facebook - Del Mare Eventos
                Instagran - Del Mare Eventos
              </div>
              <div class="block-provider">
                <h6>ILANA VENTURA</h6>
                Telefones: (81) 8678-9526 / (81) 9987-7619<br>
                Instagram: @ilanaventura<br>
                Facebook: Ilana Ventura Cantigas de Brincar<br>
                Site: www.ilanaventura.com.br<br>
                Email: ilanaventura@ilanaventura.com.br
              </div>
              <div class="block-provider">
                <h6>CAPITAIN AMERICA</h6>
                Fanpage: Heróis Kids<br>
                Instagram: heroiskids<br>
                Site: www.heroiskids.com<br>
                E-mail: heroiskids@hotmail.com<br>
                Telefones de contato: 81 99667.1202 e Whatsapp / 81 988220256
              </div>
              <div class="block-provider">
                <h6>PLOC! FÁBRICA DE ARTE</h6>
                Tel: 83.99149.0003 / 83.98802.1532<br>
                E-mail: contato@fabricaploc.com.br<br>
                Instagram: @fabricaploc<br>
                Facebook: @fabricaploc<br>
                Tema: Shows Temáticos e Animação Infantil
              </div>
              <div class="block-provider">
                <h6>BÉZER PRODUÇÕES</h6>
                Tel: 81 99755-9920 (Paloma Almeida) 81 99630-8982 (Clóvis Bézer)<br>
                Email: bezercontato@gmail.com<br>
                Instagram: @bezerproducoes<br>
                Facebook: Bézer Produções
              </div>
            </div>
          </div>
        </div>

        <div class="main-tab-content color-3" id="fornecedores-fortaleza">
          <div class="main-tab-content-container">
            <div class="block-notice block-notice-empty">
                <h3><strong>Ops!</strong></h3>
                <p>Ainda não temos fornecedor para o lugar selecionado...</p>
            </div>
          </div>
        </div>

        <div class="main-tab-content color-5" id="fornecedores-salvador">
          <div class="main-tab-content-container">
            <div class="providers">
              <div class="block-provider">
                <h6>BALCÃO GOURMET – 08 a 23 de agosto</h6>
                Tels: 9185-1929 / 9685-1929 (whatsapp) / 8700-1929<br>
                Instagram: @balcaogourmet<br>
                Face: /balcaogourmet<br>
                E-mail: balcaogourmet@hotmail.com
              </div>
              <div class="block-provider">
                <h6>BALLOONS FEST -  08 a 23 de agosto</h6>
                Tels: 3398-2714 / 3018-2112 / 9118-2391<br>
                Instagram: @balloonsfest<br>
                E-mail: contato@balloonsfest.com<br>
                Site: www.balloonsfest.com
              </div>
              <div class="block-provider">
                <h6>BUFFET TERRA DO NUNCA – 08 a 23 de agosto</h6>
                Tels: 71. 3379-6650 / 8783-8889<br>
                Facebook: bfterradonunca<br>
                Instagram: bfterradonunca<br>
                Site:www.buffetterradonunca.com.br
              </div>
              <div class="block-provider">
                <h6>BONJOUR – Espaço de eventos – 08 a 23 de agosto</h6>
                Tels: 3045-8700/9660-5330<br>
                Facebook: buffetespacobonjour<br>
                Instagram: buffet_espaco_bonjour<br>
                E-mail: espacobonjour@gmail.com
              </div>
              <div class="block-provider">
                <h6>CANETAS E CIA BRINDES EXPRESS  - 08 a 23 de agosto</h6>
                Tel: 71 3491 4131<br>
                Facebook : brindes express <br>
                Instagram: canetasecia_brindesexpress<br>
                E-mail: vendas@canetasecia.com.br<br>
                Site: www.canetasecia.com.br
              </div>
              <div class="block-provider">
                <h6>DOCICES DA VOVOQUINHA – Doces criativos – 08 a 15 de agosto</h6>
                Tels: 813-2607<br>
                E-mail: florminhas.docices@gmail.com<br>
                Instagram: docicesdavovoquinha
              </div>
              <div class="block-provider">
                <h6>CLASS CERIMONIAL E EVENTOS – 16 a 23 de agosto</h6>
                E-mail: contato.cerimonialclass@hotmail.com<br>
                Tel: 3039-0255/9214-2186/8862-9495<br>
                Facebook: /contatocerimonialclass<br>
                Instagram: @classcerimonial<br>
                Site: www.classcerimonial.com
              </div>
              <div class="block-provider">
                <h6>CLEO SILVA – Doces Criativos – 08 a 15 de agosto</h6>
                Tel: (071) 3233-5317 <br>
                E-mail: cleo_artemodelagem@yahoo.com.br <br>
                Instagram: @cleopsilva<br>
                Facebook: cleosilva.doces
              </div>
              <div class="block-provider">
                <h6>CORUJICE EVENTOS PERSONALIZADOS – Decoração – 08 a 23 de agosto</h6>
                Tel: 9906-6244<br>
                Instagram: @corujiceventos <br>
                Fan Page CorujiceEventosPersonalizados<br>
                E-mail: contato@corujiceventos.com.br<br>
                Site: www.corujiceventos.com.br
              </div>
              <div class="block-provider">
                <h6>DOCÍSSIMO – 08 a 23 de agosto</h6>
                Tel: 9306-6088<br>
                Instagram: @docissimo<br>
                E-mail: contato@docissiomo.com
              </div>
              <div class="block-provider">
                <h6>DIANA DOCES – BUFFET 16 a 23 de agosto</h6>
                Tels: 3240-5456 /9724-4122/ 8884-8051<br>
                E-mail: dri76g@hotmail.com<br>
                Instagram: dianadoces<br>
                Facebook: dianadoces.adriana
              </div>
              <div class="block-provider">
                <h6>ESTÚDIO CANGURU – Filmagem -  08 a 23 de agosto</h6>
                Tels: 8215-7466 <br>
                Instagram:@estudiocanguru<br>
                E-mail: contato@estudiocanguru.com.br<br>
                Site: www.vimeo.com/estudiocanguru
              </div>
              <div class="block-provider">
                <h6>FANTASY KIDS – Loja de Fantasia – 16 a 23 de agosto</h6>
                Tels: 3014-3955 /9960-2205<br>
                E-mail: socacaribe@hotmail.com<br>
                Instagram: fantasy.kids<br>
                Site: www.fantasy-kids.com
              </div>
              <div class="block-provider">
                <h6>INESQUECÍVEL  PARTY DESIGN – Decoração – 08 a 15 de agosto</h6>
                Tel: 99396147<br>
                Instagram: @inesquecivelparty<br>
                E-mail: m.ines.arteaga@globo.com
              </div>
              <div class="block-provider">
                <h6>ITAPARICA TOUR – Agência de Viagens</h6>
                Tel: (71) 3205-6666 <br>
                E-mail: emissivo@itapricatour.com<br>
                Site: www.itaparicatour.com.br<br>
                Fan Page: Itaparica-Tour
              </div>
              <div class="block-provider">
                <h6>KATAVENTO –Buffet – 08 a 23 de agosto</h6>
                Tels: 3316-5480 fixo / 9290-6811 tim ?8823-6369 oi<br>
                email : contato@kataventobuffetcrianca.com.br<br>
                site: www.kataventobuffetcrianca.com.br<br>
                Instagram: @kataventobuffetcrianca  <br>
                Facebook : katavento buffet criança
              </div>
              <div class="block-provider">
                <h6>LAGARTA PINTADA –Design de doces – 16 a 23 de agosto BY JAMILE FALCÃO</h6>
                Telefones: 71 3308-1494 – 71 8106-2830<br>
                E-mail: lagartapintada.buffet@hotmail.com<br>
                Instagram: @millefalcao<br>
                Blog: http://lagartapintadabuffet.blogspot.com.br<br>
                Facebook: Jamille Falcão
              </div>
              <div class="block-provider">
                <h6>LOLLIPOP PRODUÇÕES – Decoração – 16 a 23 de agosto</h6>
                Tel: 71. 8293-4880<br>
                Email: lollipopproducoes@terra.com.br <br>
                Facebook - Lollipop Produções <br>
                Instagram - @lollipopproducoes
              </div>
              <div class="block-provider">
                <h6>MAISON KATIA PELLEGRINI – Bolo – 16 a 23 de agosto</h6>
                Tels: 71 3354-4558 / 71 9919-9855 / 71 9997-1761<br>
                Instagram: @katiapellegrini<br>
                E-mail: katiapellegrini@yahoo.com.br
              </div>
              <div class="block-provider">
                <h6>MÃE ME QUER ATELIÊ – 16 a 23 de agosto</h6>
                Instagram: @maemequer @thainacgomes<br>
                E-mail: contato@maemequer.com /drathainacarneiro@gmail.com<br>
                Site: www.maemequer.com
              </div>
              <div class="block-provider">
                <h6>MUNDO CARAMELO – FESTAS PERSONALIZADAS – 08 a 23 de agosto</h6>
                Tels: (71) 8845-9082/9192-6515<br>
                E-mail: mundocaramelo@outlook.com <br>
                Facebook: mundocaramelofestas<br>
                Instagram: mundocaramelofestas
              </div>
              <div class="block-provider">
                <h6>NÓS DOIS KIDS – Fotografia – 08 a 23 de agosto</h6>
                Tels: (71)- 9618-5103 vivo 8727-1390 oi / 9386-5983 tim / 8115-1305 claro <br>
                Site: www.nos2kidsfotografiainfantil.com.br<br>
                Email: contato@agencianos2.com.br / laerciofotografo@hotmail.com
              </div>
              <div class="block-provider">
                <h6>ONE PRINT – Fotografia e Personalizados – 16 a 23 de agosto</h6>
                Tels: (71) 3489-0451 / 3248-5384 <br>
                Site: www.oneprint.com.br <br>
                Instagram: @oneprint<br>
                Facebook: oneprint <br>
                E-mail: contato@oneprint.com.br
              </div>
              <div class="block-provider">
                <h6>PEUART ATELIÊ DIGITAL – Personalizados – 16 a 23 de agosto</h6>
                Tels: (71) 3625-2482 | (71) 88679800 | Whatsapp: (71) 8675-5315<br>
                Instagram: @peuart<br>
                Fanpage: /Peuart<br>
                E-mail: peuart@hotmail.com<br>
                Site: www.peuart.com.br
              </div>
              <div class="block-provider">
                <h6>PONTO DAS FESTAS E UTILIDADES – 08 a 15 de agosto</h6>
                Tels: 9340-1527/9256-9947<br>
                Facebook: pontodasfestas<br>
                Instagram: @pontodasfestas
              </div>
              <div class="block-provider">
                <h6>STUDIO ANJUSS – Fotografia/Newborn  – 08 a 15 de agosto</h6>
                Tels: Tel: (71) 3019-3020 | WhatsApp: (71) 9157-2294<br>
                Facebook: /studioanjuss <br>
                Instagram: @studioanjuss<br>
                E-mail: studioanjuss@gmail.com<br>
                Site: www.studioanjuss.com
              </div>
              <div class="block-provider">
                <h6>RÊ MARQUES – Fotografia – 08 a 23 de agosto</h6>
                Tel: (71) 9673-7793<br>
                E-mail: contato@remarquesfotografia.com<br>
                Instagram: @remarquesfotografia<br>
                Site: www.remarquesfotografia.com
              </div>
              <div class="block-provider">
                <h6>SUPORTE EVENTOS – Locação de Mobília</h6>
                E-mail: contato2@suporteventos.com.br<br>
                Tels: 3287-1165/9212-9272 VIVO /9966-1165 VIVO/ 8610-1165 OI – WHATSAPP<br>
                Instagram: @suporteventos<br>
                Site: www.suporteventos.com.br
              </div>
              <div class="block-provider">
                <h6>TOKE KIDS –Locação – 08 a 15 de agosto</h6>
                Tels: 3381-9538/3389-9266/8814-9539<br>
                Instagram: @tokids<br>
                E-mail: glaucialopo@oi.com.br
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Sorteios -->
<div class="section-page" id="sorteios">
  <div class="section-page-container">
    <header class="section-page-header">
      <h2 class="main-heading">Sorteios</h2>
    </header>

    <div class="section-page-content">
      <div class="text-intro text">
        <p>
          Venha planejar a festa do seu filho,<br>
          feche negócio com algum de nossos expositores<br>
          e concorra a muitos prêmios.
        </p>
      </div>

      <div class="text rules">
        <a href="<?php echo $this->_asset('default/images/regulamento-sorteios.jpg'); ?>" class="lightbox"><span class="icon-search"></span> Confira o regulamento dos sorteios</a>
      </div>

      <div class="main-tab">
        <div class="main-tab-index">
          <ul class="list-colorbutton">
            <li class="color-3"><a href="#sorteios-fortaleza">Fortaleza</a></li>
            <li class="color-1"><a href="#sorteios-recife">Recife</a></li>
            <li class="color-5"><a href="#sorteios-salvador">Salvador</a></li>
          </ul>
        </div>

        <div class="main-tab-content color-4" id="sorteios-fortaleza">
          <div class="main-tab-content-container">
            <div class="award-color-3">
              <div class="award-heading">
                <span>Fortaleza</span>
              </div>
              <div class="grid grid-items-4">
                <div class="grid-item">
                  <div class="award-image">
                    <img src="<?php echo $this->_asset('default/images/sorteios/viagem-disney.png'); ?>" alt="">
                  </div>
                  <div class="award-description">Viagem para Disney</div>
                </div>
                <div class="grid-item">
                  <div class="award-image">
                    <img src="<?php echo $this->_asset('default/images/sorteios/assinatura-tv.png'); ?>" alt="">
                  </div>
                  <div class="award-description">Um ano de TV por assinatura, incluindo canais infantis</div>
                </div>
                <div class="grid-item">
                  <div class="award-image">
                    <img src="<?php echo $this->_asset('default/images/sorteios/roupa-infantil.png'); ?>" alt="">
                  </div>
                  <div class="award-description">Um ano de roupa infantil</div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="main-tab-content color-4" id="sorteios-recife">
          <div class="main-tab-content-container">
            <div class="award-color-1">
              <div class="award-heading">
                <span>Recife</span>
              </div>
              <div class="grid grid-items-4">
                <div class="grid-item">
                  <div class="award-image">
                    <img src="<?php echo $this->_asset('default/images/sorteios/viagem-disney.png'); ?>" alt="">
                  </div>
                  <div class="award-description">Viagem para Disney</div>
                </div>
                <div class="grid-item">
                  <div class="award-image">
                    <img src="<?php echo $this->_asset('default/images/sorteios/assinatura-tv.png'); ?>" alt="">
                  </div>
                  <div class="award-description">Um ano de TV por assinatura, incluindo canais infantis</div>
                </div>
                <div class="grid-item">
                  <div class="award-image">
                    <img src="<?php echo $this->_asset('default/images/sorteios/roupa-infantil.png'); ?>" alt="">
                  </div>
                  <div class="award-description">Um ano de roupa infantil</div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="main-tab-content color-4" id="sorteios-salvador">
          <div class="main-tab-content-container">
            <div class="award-color-5">
              <div class="award-heading">
                <span>Salvador</span>
              </div>
              <div class="grid grid-items-4">
                <div class="grid-item">
                  <div class="award-image">
                    <img src="<?php echo $this->_asset('default/images/sorteios/viagem-beach-park.png'); ?>" alt="">
                  </div>
                  <div class="award-description">Viagem para Beach Park (CE)</div>
                </div>
                <div class="grid-item">
                  <div class="award-image">
                    <img src="<?php echo $this->_asset('default/images/sorteios/viagem-beto-carrero.png'); ?>" alt="">
                  </div>
                  <div class="award-description">Viagem para Beto carrero World (SC)</div>
                </div>
                <div class="grid-item">
                  <div class="award-image">
                    <img src="<?php echo $this->_asset('default/images/sorteios/assinatura-tv.png'); ?>" alt="">
                  </div>
                  <div class="award-description">Um ano de TV por assinatura, incluindo canais infantis</div>
                </div>
                <div class="grid-item">
                  <div class="award-image">
                    <img src="<?php echo $this->_asset('default/images/sorteios/roupa-infantil.png'); ?>" alt="">
                  </div>
                  <div class="award-description">Um ano de roupa infantil</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Investir -->
<div class="section-page" id="investir">
  <div class="section-page-container">
    <header class="section-page-header">
      <h2 class="main-heading">Vamos crescer juntos</h2>
    </header>

    <div class="section-page-content">
      <img src="<?php echo $this->_asset('default/images/investir-decking.png'); ?>" alt="" class="decking">

      <div class="text-intro text">
        <p>Participe da Expo World Kids Brasil e tenha a sua marca divulgada para milhares de pessoas diariamente, em grandes shoppings de Salvador, Recife e Fortaleza.</p>
      </div>

      <div class="main-tab">
        <div class="grid grid-collapse grid-items-6">
          <div class="grid-item grid-item-span-2">
            <div class="main-tab-index">
              <ul class="list-colorbutton">
                <li class="color-3 on"><a href="#investir-patrocinador">Patrocinador</a></li>
                <li class="color-4"><a href="#investir-cotaouro">Cota Ouro</a></li>
                <li class="color-dark"><a href="#investir-cotaprata">Cota Prata</a></li>
                <li class="color-2"><a href="#investir-cotabronze">Cota Bronze</a></li>
                <li class="color-1"><a href="#investir-apoio">Apoio</a></li>
              </ul>
            </div>
          </div>

          <div class="grid-item grid-item-span-4">
            <div class="main-tab-content color-3" id="investir-patrocinador">
              <div class="main-tab-content-container">
                <div class="text">
                  <p>- Marca em todas as mídias, incluindo outdoors, panfletos, site, jornal, marca no portal e na plotagem do chão do evento, ação de panfletagem e ação promocional no evento;</p>
                  <p>- Espaço 3x3 - 16 dias;</p>
                  <p>- A marca no backdrop no lançamento do evento</p>
                </div>
              </div>
            </div>

            <div class="main-tab-content color-4" id="investir-cotaouro">
              <div class="main-tab-content-container">
                <div class="text">
                  <p>- Marca em todas as mídias, incluindo outdoors, panfletos, site, jornal, marca no portal e na plotagem do chão do evento, ação de panfletagem e ação promocional no evento;</p>
                  <p>- Espaço 3x3 - 16 dias;</p>
                  <p>- A marca no backdrop no lançamento do evento</p>
                </div>
              </div>
            </div>

            <div class="main-tab-content color-dark" id="investir-cotaprata">
              <div class="main-tab-content-container">
                <div class="text">
                  <p>- Marca em todas as mídias, incluindo outdoors, panfletos, site, jornal, marca no portal e na plotagem do chão do evento, ação de panfletagem e ação promocional no evento;</p>
                  <p>- Espaço 3x3 - 16 dias;</p>
                  <p>- A marca no backdrop no lançamento do evento</p>
                </div>
              </div>
            </div>

            <div class="main-tab-content color-2" id="investir-cotabronze">
              <div class="main-tab-content-container">
                <div class="text">
                  <p>- Marca em todas as mídias, incluindo outdoors, panfletos, site, jornal, marca no portal e na plotagem do chão do evento, ação de panfletagem e ação promocional no evento;</p>
                  <p>- Espaço 3x3 - 16 dias;</p>
                  <p>- A marca no backdrop no lançamento do evento</p>
                </div>
              </div>
            </div>

            <div class="main-tab-content color-1" id="investir-apoio">
              <div class="main-tab-content-container">
                <div class="text">
                  <p>- Marca em todas as mídias, incluindo outdoors, panfletos, site, jornal, marca no portal e na plotagem do chão do evento, ação de panfletagem e ação promocional no evento;</p>
                  <p>- Espaço 3x3 - 16 dias;</p>
                  <p>- A marca no backdrop no lançamento do evento</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Empresas -->
<div class="section-page" id="empresas">
  <div class="section-page-container">
    <header class="section-page-header">
      <h2 class="main-heading">Empresas</h2>
    </header>

    <div class="section-page-content">
      <div class="text-intro text">
        <p>A Expo World Kids Brasil será um grande evento voltado para empresas do <strong class="highlight">ramo de festas infantis</strong>, por isso você é parte essencial.</p>
        <p>Traga sua experiência, seus diferenciais e feche muitos negócios.</p>
      </div>
    </div>
  </div>
</div>

<!-- Parceiros -->
<div class="section-page" id="parceiros">
  <div class="section-page-container">
    <header class="section-page-header">
      <h2 class="main-heading">Parceiros patrocinadores</h2>
    </header>

    <div class="section-page-content">
      <div class="text-intro text">
        <p>Para serem realizados, os grandes eventos precisam do apoio de empresas que acreditam na sua realização. Estas são as empresas que acreditam na Expo World Kids Brasil:</p>
      </div>
      <ul class="list-logos">
        <li><img src="<?php echo $this->_asset('default/images/parceiros/balloonsfest.png'); ?>" alt=""></li>
        <li><img src="<?php echo $this->_asset('default/images/parceiros/itaparicatour.png'); ?>" alt=""></li>
        <li><img src="<?php echo $this->_asset('default/images/parceiros/kairemota.png'); ?>" alt=""></li>
        <li><img src="<?php echo $this->_asset('default/images/parceiros/suporteeventos.png'); ?>" alt=""></li>
        <li><img src="<?php echo $this->_asset('default/images/parceiros/ummaisum.png'); ?>" alt=""></li>
      </ul>
    </div>
  </div>
</div>

<!-- Orçamento -->
<div class="section-page" id="orcamento">
  <div class="section-page-container">
    <header class="section-page-header">
      <h2 class="main-heading">Orçamento</h2>
    </header>

    <div class="section-page-content">
      <form id="formBudget" method="post" action="<?php echo $this->_url('form/orcamento'); ?>" data-form-ajax class="form">
        <fieldset>
          <legend>Orçamento</legend>

          <div class="grid grid-items-2">
            <div class="grid-item">
              <h4 class="heading">Seus dados</h4>
              <label>Nome *<input name="name" type="text" required></label>
              <label>Email *<input name="email" type="email" required></label>
              <label>Telefone *<input name="phone" type="text" data-field-mask="phone" required></label>
            </div>

            <div class="grid-item">
              <h4 class="heading">Seu evento</h4>

              <label>Cidade *
                <select name="city" required>
                    <option value="Recife">Recife</option>
                    <option value="Fortaleza">Fortaleza</option>
                    <option value="Salvador">Salvador</option>
                </select>
              </label>

              <label>Categoria *
                <select name="category" required>
                  <option value="Animação">Animação</option>
                  <option value="Balões">Balões</option>
                  <option value="Bolo, doces e salgados">Bolo, doces e salgados</option>
                  <option value="Buffet e decoração">Buffet e decoração</option>
                  <option value="Casa de festas infantis">Casa de festas infantis</option>
                  <option value="Convites e lembrancinhas">Convites e lembrancinhas</option>
                  <option value="Foto e filmagem">Foto e filmagem</option>
                  <option value="Moda">Moda</option>
                  <option value="Serviços especiais">Serviços especiais</option>
                </select>
              </label>

              <div class="grid grid-items-2">
                <div class="grid-item">
                  <label>Local<input name="location" type="text"></label>
                </div>

                <div class="grid-item">
                  <label>Qtd de convidados *<input name="guests" type="number" required></label>
                </div>
              </div>

              <div class="grid grid-items-2">
                <div class="grid-item">
                  <label>Data *<input name="date" type="text" data-field-mask="date" required></label>
                </div>

                <div class="grid-item">
                  <label>Horário *<input name="time" type="text" data-field-mask="time" required></label>
                </div>
              </div>

              <label>Descrição do evento<textarea name="event" cols="30" rows="10" maxlength="600" data-field-count class="height-90"></textarea></label>
            </div>
          </div>

          <div class="block-action">
            <button type="submit" class="button button-large button-color-4">Enviar</button>
          </div>
        </fieldset>
      </form>
    </div>
  </div>
</div>

<!-- Contato -->
<div class="section-page" id="contato">
  <div class="section-page-container">
    <header class="section-page-header">
      <h2 class="main-heading">Contato</h2>
    </header>

    <div class="section-page-content">
      <div class="grid grid-items-2">
        <div class="grid-item">
          <div class="contacts">
            <div class="grid grid-items-2">
              <div class="grid-item">
                <div class="contact">
                  <h4>Grazzi Freire</h4>
                  Bahia: <a href="tel:07184362487">(71) 8436-2487</a><br>
                  Pernambuco: <a href="tel:08183595053">(81) 8359-5053</a> Oi<br>
                  Ceará: <a href="tel:08596954644">(85) 9695-4644</a> Tim<br>
                  Sergipe: <a href="tel:07998012648">(79) 9801-2648</a> Oi
                </div>
              </div>

              <div class="grid-item">
                <div class="contact">
                  <h4>Graça Dias</h4>
                  Bahia: <a href="tel:07191926515">(71) 9192-6515</a> Tim<br>
                  Pernambuco: <a href="tel:08197278224">(81) 9727-8224</a> Tim<br>
                  Ceará: <a href="tel:08592512221">(85) 9251-2221</a> Claro
                </div>
              </div>
            </div>

            <div class="links">
              <a href="http://www.expokidsbrasil.com.br">www.expokidsbrasil.com.br</a> | <a href="mailto:comercial@expokidsbrasil.com.br">comercial@expokidsbrasil.com.br</a>
            </div>
          </div>
        </div>

        <div class="grid-item">
          <form id="formContact" method="post" action="<?php echo $this->_url('form/contato'); ?>" data-form-ajax class="form">
            <fieldset>
              <legend>Contato</legend>
              <label>Nome *<input name="name" type="text" required></label>
              <label>Email *<input name="email" type="email" required></label>
              <label>Telefone *<input name="phone" type="text" data-field-mask="phone" required></label>
              <label>Mensagem *<textarea name="message" cols="30" rows="10" maxlength="600" data-field-count class="height-130" required></textarea></label>
              <div class="block-action">
                <button type="submit" class="button button-large button-color-3">Enviar</button>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
