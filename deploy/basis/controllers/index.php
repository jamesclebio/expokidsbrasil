<?php
/**
 * Basis
 *
 * @link https://github.com/jamesclebio/basis
 * @author James Clébio <jamesclebio@gmail.com>
 * @license http://jamesclebio.mit-license.org
 */

namespace Basis\Controllers;

class Index extends \Basis\Core\Controller
{
  public function index() {
    $this->setLayout('default');
    $this->setView('index');
    $this->setTitle('Expo World Kids Brasil');
    $this->setDescription('');
    $this->setAnalytics(true);
  }
}
