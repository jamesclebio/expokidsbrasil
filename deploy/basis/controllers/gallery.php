<?php
/**
 * Basis
 *
 * @link https://github.com/jamesclebio/basis
 * @author James Clébio <jamesclebio@gmail.com>
 * @license http://jamesclebio.mit-license.org
 */

namespace Basis\Controllers;

class Gallery extends \Basis\Core\Controller
{
  public function index() {
    $this->setLayout(null);
    $this->setView(null);
  }

  public function view() {
    $this->gallery = new \Basis\Helpers\Gallery();
    $this->gallery->setName($this->_get('album'));
    $this->gallery->setPath('assets/default/images/galeria/' . $this->_get('album') . '/');
    $this->gallery->builder();
  }
}
