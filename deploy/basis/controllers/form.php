<?php
/**
 * Basis
 *
 * @link https://github.com/jamesclebio/basis
 * @author James Clébio <jamesclebio@gmail.com>
 * @license http://jamesclebio.mit-license.org
 */

namespace Basis\Controllers;

class Form extends \Basis\Core\Controller
{
  public function index() {
    $this->setLayout(null);
    $this->setView(null);
  }

  public function orcamento() {
    if ($_POST) {
      $sender = 'contato@expokidsbrasil.com.br';
      $mail = new \Basis\Helpers\Mail();

      $mail->setHeader(
          'From: ' . $_POST['name'] . ' <' . $sender . '>' . "\r\n" .
          'Return-Path: ' . $sender . "\r\n" .
          'Reply-To: ' . $_POST['email'] . "\r\n"
      );

      // $mail->setTo('dev@jamesclebio.com.br');
      $mail->setTo('contato@expokidsbrasil.com.br');
      $mail->setSubject('[Expo Kids World Brasil] Orçamento');
      $mail->setBody(
          'Nome: ' . $_POST['name'] . "\n" .
          'E-mail: ' . $_POST['email'] . "\n" .
          'Telefone: ' . $_POST['phone'] . "\n" .
          '---' . "\n\n" .
          'Cidade: ' . $_POST['city'] . "\n" .
          'Categoria: ' . $_POST['category'] . "\n" .
          'Local: ' . $_POST['location'] . "\n" .
          'Qts de convidados: ' . $_POST['guests'] . "\n" .
          'Data: ' . $_POST['date'] . "\n" .
          'Horário: ' . $_POST['time'] . "\n" .
          '---' . "\n\n" .
          $_POST['event'] . "\n\n" .
          '---' . "\n" .
          'Mensagem enviada a partir de ' . $_SERVER['HTTP_HOST'] . ' em ' . date('d/m/Y H:i:s')
      );
      $mail->send($this, null, null, '-r' . $sender);
    }
  }

  public function contato() {
    if ($_POST) {
      $sender = 'contato@expokidsbrasil.com.br';
      $mail = new \Basis\Helpers\Mail();

      $mail->setHeader(
          'From: ' . $_POST['name'] . ' <' . $sender . '>' . "\r\n" .
          'Return-Path: ' . $sender . "\r\n" .
          'Reply-To: ' . $_POST['email'] . "\r\n"
      );

      // $mail->setTo('dev@jamesclebio.com.br');
      $mail->setTo('contato@expokidsbrasil.com.br');
      $mail->setSubject('[Expo Kids World Brasil] Contato');
      $mail->setBody(
          'Nome: ' . $_POST['name'] . "\n" .
          'E-mail: ' . $_POST['email'] . "\n" .
          'Telefone: ' . $_POST['phone'] . "\n" .
          '---' . "\n\n" .
          $_POST['message'] . "\n\n" .
          '---' . "\n" .
          'Mensagem enviada a partir de ' . $_SERVER['HTTP_HOST'] . ' em ' . date('d/m/Y H:i:s')
      );
      $mail->send($this, null, null, '-r' . $sender);
    }
  }
}
