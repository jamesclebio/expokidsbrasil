<?php
/**
 * Basis
 *
 * @link https://github.com/jamesclebio/basis
 * @author James Clébio <jamesclebio@gmail.com>
 * @license http://jamesclebio.mit-license.org
 */

namespace Basis\Core;

class Autoload
{
  static public function init($query) {
    $query = strtolower(str_replace('\\', '/', $query)) . '.php';

    if (file_exists($query)) {
      require_once($query);
    }
  }
}
