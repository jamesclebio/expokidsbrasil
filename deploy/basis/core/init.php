<?php
/**
 * Basis
 *
 * @link https://github.com/jamesclebio/basis
 * @author James Clébio <jamesclebio@gmail.com>
 * @license http://jamesclebio.mit-license.org
 */

namespace Basis\Core;

class Init
{
  private $root;
  private $get;
  private $controller;
  private $action;

  public function __construct($routeController = array()) {
    $this->setRoot();
    $this->querystringExplode();
    $this->setController();
    $this->setAction();
    $this->querystringHook();
    $this->getController($routeController);
    $this->page();
  }

  private function setRoot() {
    $path = pathinfo($_SERVER['SCRIPT_NAME']);
    $this->root = ($path['dirname'] == '/') ? $path['dirname'] : $path['dirname'] . '/';
  }

  private function querystringExplode() {
    $get = (isset($_GET['get'])) ? $_GET['get'] : 'index';
    $this->get = explode('/', $get);
  }

  private function querystringHook() {
    $keyList = array();
    $valueList = array();
    $index = 0;

    unset($this->get[0], $this->get[1]);

    if (!empty($this->get)) {
      if (end($this->get) == null) {
        array_pop($this->get);
      }

      foreach ($this->get as $value) {
        if ($index % 2 == 0) {
          array_push($keyList, $value);
        } else {
          array_push($valueList, $value);
        }

        $index++;
      }
    }

    if (!empty($keyList)) {
      if (count($keyList) > count($valueList)) {
        array_push($valueList, null);
      }

      $this->get = array_combine($keyList, $valueList);
    } else {
      $this->get = array();
    }
  }

  private function getController($routeController) {
    $controller = CONTROLLERS . str_replace('-', '', $this->controller) . '.php';

    if (!file_exists($controller)) {
      if (array_key_exists($this->controller, $routeController)) {
        $this->controller = str_replace('-', '', $routeController[$this->controller]);
        $controller = CONTROLLERS . $this->controller . '.php';
      } else {
        header('HTTP/1.0 404 Not Found');
        readfile('404.html');
        exit;
      }
    } else {
      $this->controller = str_replace('-', '', $this->controller);
    }

    require_once $controller;
  }

  private function setController() {
    $this->controller = $this->get[0];
  }

  private function getAction($page) {
    $action = $this->action;

    if ($action) {
      if (!method_exists($page, $action)) {
        exit('URL error: no action');
      }

      $page->$action();
    }
  }

  private function setAction() {
    if (!isset($this->get[1]) || $this->get[1] == null || $this->get[1] == 'index') {
      $this->action = false;
    } else {
      $this->action = str_replace('-', '', $this->get[1]);
    }
  }

  private function page() {
    $controller = '\Basis\Controllers\\' . $this->controller;
    $controller = new $controller;

    $controller->setRoot($this->root);
    $controller->setGet($this->get);
    $controller->setNameController($this->controller);
    $controller->setNameAction($this->action);
    $controller->index();
    $controller->getSession();
    $controller->getAuthentication();
    $this->getAction($controller);
    $controller->getLayout();
    $controller->getView();
  }
}
