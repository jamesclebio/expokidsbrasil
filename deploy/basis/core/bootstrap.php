<?php
/**
 * Basis
 *
 * @link https://github.com/jamesclebio/basis
 * @author James Clébio <jamesclebio@gmail.com>
 * @license http://jamesclebio.mit-license.org
 */

namespace Basis\Core;

const MODELS = 'basis/models/',
      VIEWS = 'basis/views/',
      CONTROLLERS = 'basis/controllers/',
      CACHE = 'basis/cache/',
      LAYOUTS = 'basis/layouts/',
      ASSETS = 'assets/';

require_once 'autoload.php';

set_include_path('basis/views/includes/');
date_default_timezone_set(TIMEZONE);
spl_autoload_register('Basis\Core\Autoload::init');

new Init($routeController);
