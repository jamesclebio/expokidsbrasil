<?php
/**
 * Basis
 *
 * @link https://github.com/jamesclebio/basis
 * @author James Clébio <jamesclebio@gmail.com>
 * @license http://jamesclebio.mit-license.org
 */

namespace Basis\Core;

class Controller
{
  private $nameController;
  private $nameAction;
  private $root;
  private $get;
  private $session = false;
  private $authentication = false;
  private $layout = 'default';
  private $view = 'index';
  private $title;
  private $description;
  private $htmlAttribute;
  private $htmlClass;
  private $headAppend;
  private $bodyAttribute;
  private $bodyClass;
  private $bodyPrepend;
  private $bodyAppend;
  private $analytics = false;

  public function getNameController() {
    return $this->nameController;
  }

  public function setNameController($nameController) {
    $this->nameController = $nameController;
  }

  public function getNameAction() {
    return $this->nameAction;
  }

  public function setNameAction($nameAction) {
    $this->nameAction = $nameAction;
  }

  public function setRoot($root) {
    $this->root = $root;
  }

  public function setGet($get) {
    $this->get = $get;
  }

  public function getSession() {
    if ($this->session) {
      session_start();
    }
  }

  protected function setSession($session = false) {
    $this->session = $session;
  }

  public function getAuthentication() {
    if ($this->authentication) {
      \Basis\Helpers\Authentication::sessionValidate();
    }
  }

  protected function setAuthentication($authentication = false) {
    $this->authentication = $authentication;
  }

  public function getLayout() {
    if ($this->layout) {
      require_once LAYOUTS . $this->layout . '.php';
    }
  }

  protected function setLayout($layout) {
    $this->layout = $layout;
  }

  public function getView() {
    if ($this->view) {
      require_once VIEWS . $this->view . '.php';
    }
  }

  protected function setView($view) {
    $this->view = $view;
  }

  protected function getTitle() {
    return $this->title;
  }

  protected function setTitle($title) {
    $this->title = $title;
  }

  protected function getDescription() {
    return $this->description;
  }

  protected function setDescription($description) {
    $this->description = $description;
  }

  protected function getHtmlAttribute() {
    echo ' ', $this->htmlAttribute;
  }

  protected function setHtmlAttribute($htmlAttribute) {
    $this->htmlAttribute = $htmlAttribute;
  }

  protected function getHtmlClass() {
    echo $this->htmlClass;
  }

  protected function setHtmlClass($htmlClass) {
    $this->htmlClass = $htmlClass;
  }

  protected function getBodyAttribute() {
    echo ' ', $this->bodyAttribute;
  }

  protected function setBodyAttribute($bodyAttribute) {
    $this->bodyAttribute = $bodyAttribute;
  }

  protected function getBodyClass() {
    echo $this->bodyClass;
  }

  protected function setBodyClass($bodyClass) {
    $this->bodyClass = $bodyClass;
  }

  protected function getHeadAppend() {
    echo $this->headAppend;
  }

  protected function setHeadAppend($headAppend) {
    $this->headAppend = $headAppend;
  }

  protected function getBodyPrepend() {
    echo $this->bodyPrepend;
  }

  protected function setBodyPrepend($bodyPrepend) {
    $this->bodyPrepend = $bodyPrepend;
  }

  protected function getBodyAppend() {
    echo $this->bodyAppend;
  }

  protected function setBodyAppend($bodyAppend) {
    $this->bodyAppend = $bodyAppend;
  }

  protected function getAlert() {
    if (!empty($_SESSION['alert'])) {
      echo '<div class="alert alert-' . $_SESSION['alert']['type'] . '"><a href="#" class="close" title="Fechar alerta">x</a>' . $_SESSION['alert']['content'] . '</div>';
      $_SESSION['alert'] = '';
    }
  }

  public function setAlert($content = null, $type = 'warning') {
    $_SESSION['alert'] = array('content' => $content, 'type' => $type);
  }

  protected function getAnalytics() {
    if ($this->analytics) {
      include 'analytics.php';
    }
  }

  protected function setAnalytics($analytics = false) {
    $this->analytics = $analytics;
  }

  public function _get($index = false) {
    if ($index) {
      return (array_key_exists($index, $this->get)) ? $this->get[$index] : null;
    } else {
      return $this->get;
    }
  }

  public function _url($target = null) {
    return ($target != 'root') ? $this->root . $target : $this->root;
  }

  public function _asset($target = null) {
    return $this->root . ASSETS . $target;
  }

  public function _include($target = null) {
    return $this->root . INCLUDES . $target;
  }

  protected function _cache() {
    file_put_contents(CACHE . 'cache-' . $this->name, serialize($this));
  }
}
