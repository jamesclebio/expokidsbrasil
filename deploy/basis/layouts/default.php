<!doctype html>
<html<?php $this->getHtmlAttribute(); ?> class="<?php $this->getHtmlClass(); ?> no-js" lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $this->getTitle(); ?></title>
  <meta name="description" content="<?php echo $this->getDescription(); ?>">
  <meta name="robots" content="noindex">
  <link rel="stylesheet" href="<?php echo $this->_asset('default/styles/main.css'); ?>">
  <script src="<?php echo $this->_asset('default/scripts/head.js'); ?>"></script>
  <?php $this->getHeadAppend(); ?>
</head>
<body<?php $this->getBodyAttribute(); ?> class="<?php $this->getBodyClass(); ?>">
  <?php $this->getAnalytics(); ?>
  <?php $this->getBodyPrepend(); ?>

  <header class="default-header">
    <div class="default-header-container">
      <h1 class="main-logo"><a href="#!/home">Expo World Kids Brasil</a></h1>
      <nav class="main-nav">
        <ul class="primary">
          <li><a href="#!/home">Home</a></li>
          <li><a href="#!/evento">O evento</a></li>
          <li><a href="#!/programacao">Programação</a></li>
          <li><a href="#!/galeria">Galeria</a></li>
          <li><a href="#!/fornecedores">Fornecedores</a></li>
          <li><a href="#!/sorteios">Sorteios</a></li>
          <li><a href="#!/investir">Investir</a></li>
          <li><a href="#!/parceiros">Parceiros</a></li>
        </ul>

        <ul class="secondary">
          <li><a href="#!/orcamento">Solicite orçamento</a></li>
          <li><a href="#!/contato">Fale conosco</a></li>
        </ul>
      </nav>
    </div>
  </header>

  <?php $this->getView(); ?>

  <button type="button" data-toggle-roll="40|display-block" data-roll-top class="default-roll-top" title="Ir para o topo"></button>

  <script src="<?php echo $this->_asset('default/scripts/main.js'); ?>"></script>
  <?php $this->getBodyAppend(); ?>
</body>
</html>
