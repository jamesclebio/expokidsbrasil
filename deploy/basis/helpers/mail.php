<?php
/**
 * Basis
 *
 * @link https://github.com/jamesclebio/basis
 * @author James Clébio <jamesclebio@gmail.com>
 * @license http://jamesclebio.mit-license.org
 */

namespace Basis\Helpers;

class Mail
{
  private $header;
  private $to;
  private $subject;
  private $body;
  private $alert_success = '<p><strong>Email enviado com sucesso!</strong></p>';
  private $alert_error = '<p><strong>Ops! Algo deu errado...</strong></p><p>Por favor tente novamente.</p>';

  public function setHeader($header) {
    $this->header = $header;
  }

  public function setTo($to) {
    $this->to = $to;
  }

  public function setSubject($subject) {
    $this->subject = $subject;
  }

  public function setBody($body) {
    $this->body = $body;
  }

  public function setAlertSuccess($alert_success) {
    $this->alert_success = $alert_success;
  }

  public function setAlertError($alert_error) {
    $this->alert_error = $alert_error;
  }

  public function send($page, $url_success = null, $url_error = null, $postfix = null) {
    $send = mail($this->to, $this->subject, $this->body, $this->header, $postfix);

    if ($send) {
      $page->setAlert($this->alert_success, 'success');

      if ($url_success) {
        header('Location: ' . $url_success);
        exit;
      }
    } else {
      $page->setAlert($this->alert_error, 'error');

      if ($url_error) {
        header('Location: ' . $url_error);
        exit;
      }
    }
  }
}
