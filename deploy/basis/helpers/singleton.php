<?php
/**
 * Basis
 *
 * @link https://github.com/jamesclebio/basis
 * @author James Clébio <jamesclebio@gmail.com>
 * @license http://jamesclebio.mit-license.org
 */

namespace Basis\Helpers;

class Singleton
{
  public static function getInstance() {
    static $instance = null;

    if (null === $instance) {
      $instance = new static();
    }

    return $instance;
  }

  private function __construct() {}

  private function __clone() {}

  private function __wakeup() {}
}
