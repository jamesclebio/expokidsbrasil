<?php
/**
 * Basis
 *
 * @link https://github.com/jamesclebio/basis
 * @author James Clébio <jamesclebio@gmail.com>
 * @license http://jamesclebio.mit-license.org
 */

namespace Basis\Helpers;

class Gallery
{
  private $name = 'gallery';
  private $fileTypes = array('jpg', 'jpeg', 'png');

  public function setName($name) {
    $this->name = $name;
  }

  public function setPath($path) {
    $this->path = $path;
  }

  public function builder() {
    $directory = @opendir($this->path);

    if ($directory) {
      echo '<ul class="list-thumbs">';

      while ($file = readdir($directory)) {
        $imageLarge = $this->path . $file;
        $imageThumb = $this->path . 'thumbs/' . $file;

        if (in_array(substr(strtolower($file), strrpos($file, '.') + 1), $this->fileTypes)) {
          echo '<li><a href="' . $imageLarge . '" rel="' . $this->name . '" class="lightbox"><img src="' . $imageThumb . '" alt=""></a></li>';
        }
      }

      echo '</ul>';

      closedir($directory);
    } else {
      echo '<div class="block-notice block-notice-empty"><h3><strong>Ops!</strong></h3><p>Nenhuma imagem para mostrar aqui...</p></div>';
    }
  }
}
