# Basis

## Requirements

* [Apache](http://www.apache.org/)
* [PHP (>=5.4)](http://www.php.net/)
* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/)
* [npm](https://www.npmjs.org/)
* [Grunt](http://gruntjs.com/)
* [Bower](http://bower.io/)
* [Ruby](https://www.ruby-lang.org/)
* [Compass](http://compass-style.org/)

## Includes

* [Ionicons](http://ionicons.com/)
* [Open Sans](http://www.google.com/fonts/specimen/Open+Sans)
* [Apache Server Configs](https://github.com/h5bp/server-configs-apache)
* [Modernizr](http://modernizr.com/)
* [jQuery](http://jquery.com/)
* [Highcharts](http://www.highcharts.com/)
* [Masked Input](http://digitalbush.com/projects/masked-input-plugin/)
* [DateTimePicker](http://xdsoft.net/jqplugins/datetimepicker/)
* [carouFredSel](https://github.com/gilbitron/carouFredSel)
* [Colorbox](http://www.jacklmoore.com/colorbox/)

## License

[MIT License](http://jamesclebio.mit-license.org/) © James Clébio

