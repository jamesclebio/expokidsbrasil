module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		project: {
			banner:	'/* Built on <%= grunt.template.today("dddd, mmmm dS, yyyy, h:MM:ss TT") %> */',

			basis: {
				name: 'basis',
				version: '0.1.0',
				url: 'https://github.com/jamesclebio/basis',
				author: 'James Clébio <jamesclebio@gmail.com>',
				license: 'http://jamesclebio.mit-license.org/',
				scripts: {
					base: [
						'bower_components/jquery/dist/jquery.js',
						'bower_components/highcharts/highcharts.js',
						'bower_components/jquery-maskedinput/dist/jquery.maskedinput.js',
						'bower_components/jquery-maskmoney/dist/jquery.maskMoney.js',
						'bower_components/datetimepicker/jquery.datetimepicker.js',
						'bower_components/carouFredSel/jquery.carouFredSel-6.2.1.js',
						'bower_components/colorbox/jquery.colorbox-min.js',
						'bower_components/colorbox/i18n/jquery.colorbox-pt-br.js',
						'bower_components/sifter/sifter.js',
						'bower_components/microplugin/src/microplugin.js',
						'bower_components/selectize/dist/js/selectize.js'
					],

					core: [
						'assets/basis/scripts/basis.js',
						'assets/basis/scripts/basis.*.js'
					],

					main: [
						'<%= project.basis.scripts.base %>',
						'<%= project.basis.scripts.core %>',
						'assets/basis/scripts/init.js'
					]
				},

				styles: {
					path: 'assets/basis/styles',
					main: '<%= project.basis.styles.path %>/**/*.sass'
				}
			},

			default: {
				scripts: {
					base: [],

					head: [
						'bower_components/modernizr/modernizr.js',
						'assets/default/scripts/head.js'
					],

					main: [
						'<%= project.basis.scripts.main %>',
						'<%= project.default.scripts.base %>',
						'assets/default/scripts/main.*.js',
						'assets/default/scripts/main.js'
					]
				},

				styles: {
					path: 'assets/default/styles',
					main: '<%= project.default.styles.path %>/**/*.sass'
				}
			}
		},

		jshint: {
			options: {
				force: true
			},

			basis: 'assets/basis/scripts/**/*',
			default: 'assets/default/scripts/**/*'
		},

		uglify: {
			options: {
				banner:	'<%= project.banner %>\n'
			},

			basis: {
				files: {
					'deploy/assets/basis/scripts/main.js': '<%= project.basis.scripts.main %>'
				}
			},

			default: {
				files: {
					'deploy/assets/default/scripts/head.js': '<%= project.default.scripts.head %>',
					'deploy/assets/default/scripts/main.js': '<%= project.default.scripts.main %>'
				}
			}
		},

		imagemin: {
			default: {
				files: [{
					expand: true,
					cwd: 'assets/default/images/',
					src: '**/*.{png,jpg,gif}',
					dest: 'deploy/assets/default/images/'
				}]
			}
		},

		compass: {
			options: {
				banner:	'<%= project.banner %>',
				relativeAssets: true,
				noLineComments: true,
				outputStyle: 'compressed',
				raw: 'preferred_syntax = :sass'
			},

			basis: {
				options: {
					sassDir: '<%= project.basis.styles.path %>',
					specify: '<%= project.basis.styles.main %>',
					cssDir: 'deploy/assets/basis/styles',
					fontsDir: 'deploy/assets/basis/fonts'
				}
			},

			default: {
				options: {
					sassDir: '<%= project.default.styles.path %>',
					specify: '<%= project.default.styles.main %>',
					cssDir: 'deploy/assets/default/styles',
					fontsDir: 'deploy/assets/default/fonts',
					imagesDir: 'deploy/assets/default/images'
				}
			}
		},

		concat: {
			options: {
				banner:	'<%= project.banner %>\n'
			},

			basis_scripts_source: {
				files: {
					'deploy/assets/basis/scripts/main-source.js': '<%= project.basis.scripts.main %>'
				}
			},

			default_scripts_source: {
				files: {
					'deploy/assets/default/scripts/head-source.js': '<%= project.default.scripts.head %>',
					'deploy/assets/default/scripts/main-source.js': '<%= project.default.scripts.main %>'
				}
			},
		},

		copy: {
			basis_fonts: {
				expand: true,
				cwd: 'assets/basis/fonts/',
				src: '**',
				dest: 'deploy/assets/basis/fonts/'
			},

			default_fonts: {
				expand: true,
				flatten: true,
				src: 'assets/{basis, default}/fonts/**',
				dest: 'deploy/assets/default/fonts/',
				filter: 'isFile'
			}
		},

		clean: {
			basis_assets: 'deploy/assets/basis',
			default_assets: 'deploy/assets/default'
		},

		'string-replace': {
			basis: {
				files: {
					'deploy/assets/': 'deploy/assets/**/{scripts, styles}/*'
				},
				options: {
					replacements: [{
						pattern: '{{basis.name}}',
						replacement: '<%= project.basis.name %>'
					}, {
						pattern: '{{basis.version}}',
						replacement: '<%= project.basis.version %>'
					}, {
						pattern: '{{basis.url}}',
						replacement: '<%= project.basis.url %>'
					}, {
						pattern: '{{basis.author}}',
						replacement: '<%= project.basis.author %>'
					}, {
						pattern: '{{basis.license}}',
						replacement: '<%= project.basis.license %>'
					}]
				}
			}
		},

		rsync: {
			options: {
				args: ['--verbose'],
				recursive: true
			},

			deploy: {
				options: {
					src: 'deploy/assets/default/',
					dest: '/Volumes/jc/base/static/base/'
				}
			}
		},

		watch: {
			options: {
				livereload: true,
				spawn: false
			},

			gruntfile: {
				files: 'Gruntfile.js',
				tasks: 'default'
			},

			scripts: {
				files: ['<%= project.basis.scripts.main %>'],
				tasks: ['scripts']
			},

			styles: {
				files: ['<%= project.basis.styles.main %>'],
				tasks: ['styles']
			},

			default_scripts: {
				files: ['<%= jshint.default %>'],
				tasks: ['default-scripts']
			},

			default_styles: {
				files: ['<%= project.default.styles.main %>'],
				tasks: ['default-styles']
			},

			app: {
				files: ['deploy/app/**/*']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-string-replace');
	grunt.loadNpmTasks('grunt-rsync');

	// Tasks
	grunt.registerTask('default', ['jshint:basis', 'default-build']);
	grunt.registerTask('scripts', ['jshint:basis', 'default-scripts']);
	grunt.registerTask('styles', ['default-styles']);
	grunt.registerTask('deploy', ['rsync:deploy']);

	// Tasks: basis
	grunt.registerTask('basis-scripts', ['jshint:basis', 'uglify:basis', 'string-replace']);
	grunt.registerTask('basis-source', ['jshint:basis', 'concat:basis_scripts_source']);
	grunt.registerTask('basis-styles', ['compass:basis', 'string-replace']);
	grunt.registerTask('basis-build', ['basis-scripts', 'copy:basis_fonts', 'basis-styles']);

	// Tasks: default
	grunt.registerTask('default-scripts', ['jshint:default', 'uglify:default', 'string-replace']);
	grunt.registerTask('default-source', ['jshint:default', 'concat:default_scripts_source']);
	grunt.registerTask('default-styles', ['compass:default', 'string-replace']);
	grunt.registerTask('default-build', ['default-scripts', 'copy:default_fonts', 'imagemin:default', 'default-styles']);
};
